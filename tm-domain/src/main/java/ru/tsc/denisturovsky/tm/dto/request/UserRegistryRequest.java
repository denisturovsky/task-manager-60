package ru.tsc.denisturovsky.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserRegistryRequest extends AbstractUserRequest {

    @Nullable
    private String email;

    @Nullable
    private String login;

    @Nullable
    private String password;

    public UserRegistryRequest(@Nullable String token) {
        super(token);
    }

}
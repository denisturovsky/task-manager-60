package ru.tsc.denisturovsky.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskStartByIdResponse extends AbstractResponse {

}

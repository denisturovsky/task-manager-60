package ru.tsc.denisturovsky.tm.listeneer.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.denisturovsky.tm.dto.request.ApplicationAboutRequest;
import ru.tsc.denisturovsky.tm.dto.response.ApplicationAboutResponse;
import ru.tsc.denisturovsky.tm.event.ConsoleEvent;

import java.util.Locale;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info";

    @NotNull
    public static final String NAME = "about";

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = systemEndpoint.getApplicationAbout(request);
        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        System.out.format("Name: %s \n", response.getName());
        System.out.format("E-mail: %s \n", response.getEmail());
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}

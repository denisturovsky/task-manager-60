package ru.tsc.denisturovsky.tm.listeneer.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.denisturovsky.tm.event.ConsoleEvent;
import ru.tsc.denisturovsky.tm.listeneer.AbstractListener;

import java.util.Locale;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show command list";

    @NotNull
    public static final String NAME = "commands";

    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        for (@Nullable final AbstractListener listener : listeners) {
            System.out.println(listener.getName());
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}

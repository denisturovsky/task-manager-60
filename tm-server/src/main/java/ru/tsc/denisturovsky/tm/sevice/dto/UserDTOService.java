package ru.tsc.denisturovsky.tm.sevice.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.denisturovsky.tm.api.service.IServerPropertyService;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.exception.entity.UserNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.*;
import ru.tsc.denisturovsky.tm.exception.user.LoginExistsException;
import ru.tsc.denisturovsky.tm.exception.user.RoleEmptyException;
import ru.tsc.denisturovsky.tm.util.HashUtil;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserDTOService extends AbstractDTOService<UserDTO, IUserDTORepository> implements IUserDTOService {

    @Nullable
    @Autowired
    private IUserDTORepository repository;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IServerPropertyService propertyService;

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email == null ? "" : email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    protected IUserDTORepository getRepository() {
        return repository;
    }

    @Override
    public Boolean isEmailExists(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        return (repository.findByEmail(email) != null);
    }

    @Override
    public Boolean isLoginExists(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        return (repository.findByLogin(login) != null);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.update(user);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws Exception {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskService.clear(userId);
        projectService.clear(userId);
        remove(user);
    }

    @Override
    @Transactional
    public void setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.update(user);
    }

    @Override
    @Transactional
    public void userUpdate(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        repository.update(user);
    }

}
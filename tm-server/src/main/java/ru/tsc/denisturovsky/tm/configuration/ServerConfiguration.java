package ru.tsc.denisturovsky.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.denisturovsky.tm.api.service.IServerPropertyService;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.denisturovsky.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IServerPropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDBDriver());
        dataSource.setUrl(propertyService.getDBUrl());
        dataSource.setUsername(propertyService.getDBUser());
        dataSource.setPassword(propertyService.getDBPassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean =
                new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.tsc.denisturovsky.tm");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getDBDialect());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, propertyService.getDBShowSql());
        properties.put(Environment.FORMAT_SQL, propertyService.getDBFormatSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDBSecondLvlCache());
        properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getDBFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, propertyService.getDBUseQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getDBUseMinPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getDBRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDBConfigFilePath());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
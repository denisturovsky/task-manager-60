package ru.tsc.denisturovsky.tm.sevice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.repository.model.IRepository;
import ru.tsc.denisturovsky.tm.api.service.model.IService;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.exception.field.IdEmptyException;
import ru.tsc.denisturovsky.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) throws Exception {
        getRepository().add(model);
        return model;
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        getRepository().clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsById(id);
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        return getRepository().findAll(comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) throws Exception {
        if (sort == null) return findAll();
        return getRepository().findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(id);
    }

    @Nullable
    protected abstract IRepository<M> getRepository();

    @Override
    public int getSize() throws Exception {
        return getRepository().getSize();
    }

    @Override
    @Transactional
    public void remove(@Nullable final M model) throws Exception {
        if (model == null) return;
        getRepository().remove(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws Exception {
        @Nullable M result = findOneById(id);
        remove(result);
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) throws Exception {
        if (model == null) return;
        getRepository().update(model);
    }

}
package ru.tsc.denisturovsky.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    List<M> findAll() throws Exception;

    @Nullable
    List<M> findAll(@Nullable Comparator comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    @NotNull
    EntityManager getEntityManager();

    int getSize() throws Exception;

    void remove(@NotNull M model) throws Exception;

    void update(@NotNull M model) throws Exception;

}

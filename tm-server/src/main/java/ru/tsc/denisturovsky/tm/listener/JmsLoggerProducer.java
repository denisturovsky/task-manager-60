package ru.tsc.denisturovsky.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.service.IServerPropertyService;
import ru.tsc.denisturovsky.tm.log.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ru.tsc.denisturovsky.tm.context.ServerContext.CONTEXT;

@Getter
public final class JmsLoggerProducer {

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    private static final IServerPropertyService propertyService = CONTEXT.getBean(IServerPropertyService.class);

    @NotNull
    private static final String URL =
            "tcp://" + propertyService.getJMSServerHost() + ":" + propertyService.getJMSServerPort();

    @NotNull
    private final BrokerService broker = new BrokerService();

    @NotNull
    private final Connection connection;

    @NotNull
    private final ConnectionFactory serverConnectionFactory = new ActiveMQConnectionFactory(URL);

    @NotNull
    private final Queue destination;

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final MessageProducer messageProducer;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final Session session;

    @SneakyThrows
    public JmsLoggerProducer() {
        broker.addConnector(URL);
        broker.start();
        connection = serverConnectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
        messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        @NotNull final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
    }

    @SneakyThrows
    public void send(@NotNull final OperationEvent operationEvent) {
        executorService.submit(() -> sync(operationEvent));
    }

    @SneakyThrows
    private void sync(@NotNull final OperationEvent operationEvent) {
        @NotNull final Class<?> entityClass = operationEvent.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
            @NotNull final Table table = (Table) annotation;
            operationEvent.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(operationEvent));
    }

}
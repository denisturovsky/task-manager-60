package ru.tsc.denisturovsky.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.repository.model.ITaskRepository;
import ru.tsc.denisturovsky.tm.model.Task;

import java.util.Date;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    @Transactional
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Task task = new Task(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final Task task = new Task(name);
        return add(userId, task);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) throws Exception {
        @NotNull final Task task = new Task(name, description, dateBegin, dateEnd);
        return add(userId, task);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws Exception {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId";
        return entityManager.createQuery(jpql, getEntityClass())
                            .setParameter("userId", userId)
                            .setParameter("projectId", projectId)
                            .getResultList();
    }

}
package ru.tsc.denisturovsky.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;

import java.util.Date;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) throws Exception {
        @NotNull final ProjectDTO project = new ProjectDTO(name, description, dateBegin, dateEnd);
        return add(userId, project);
    }

}

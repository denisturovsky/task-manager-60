package ru.tsc.denisturovsky.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.repository.model.IProjectRepository;
import ru.tsc.denisturovsky.tm.model.Project;

import java.util.Date;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    @Transactional
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Date dateBegin,
            @NotNull final Date dateEnd
    ) throws Exception {
        @NotNull final Project project = new Project(name, description, dateBegin, dateEnd);
        return add(userId, project);
    }

}

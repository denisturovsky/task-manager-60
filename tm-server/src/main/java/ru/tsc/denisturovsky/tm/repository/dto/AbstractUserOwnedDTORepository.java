package ru.tsc.denisturovsky.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.tsc.denisturovsky.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<M> implements IUserOwnedDTORepository<M> {

    @NotNull
    @Override
    @Transactional
    public M add(
            @NotNull final String userId,
            @NotNull final M model
    ) throws Exception {
        model.setUserId(userId);
        entityManager.persist(model);
        return model;
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "DELETE FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                     .setParameter("userId", userId)
                     .executeUpdate();
    }

    @Override
    public boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql =
                "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getEntityClass())
                            .setParameter("userId", userId)
                            .getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(
            @NotNull final String userId,
            @Nullable final Comparator comparator
    ) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        @NotNull final Root<M> root = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws Exception {
        @NotNull final String jpql =
                "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getEntityClass())
                            .setParameter("userId", userId)
                            .setParameter("id", id)
                            .setMaxResults(1)
                            .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String jpql =
                "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                            .setParameter("userId", userId)
                            .getSingleResult().intValue();
    }

    @Override
    @Transactional
    public void remove(
            @NotNull final String userId,
            @NotNull final M model
    ) throws Exception {
        model.setUserId(userId);
        entityManager.remove(entityManager.merge(model));
    }

    @Override
    @Transactional
    public void update(
            @NotNull final String userId,
            @NotNull final M model
    ) throws Exception {
        model.setUserId(userId);
        entityManager.merge(model);
    }

}
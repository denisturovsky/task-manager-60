package ru.tsc.denisturovsky.tm.sevice.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.denisturovsky.tm.api.repository.model.ISessionRepository;
import ru.tsc.denisturovsky.tm.api.service.model.ISessionService;
import ru.tsc.denisturovsky.tm.model.Session;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @Nullable
    @Autowired
    private ISessionRepository repository;

    @Nullable
    protected ISessionRepository getRepository() {
        return repository;
    }

}
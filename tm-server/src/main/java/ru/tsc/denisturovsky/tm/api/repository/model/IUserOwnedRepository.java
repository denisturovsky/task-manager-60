package ru.tsc.denisturovsky.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(
            @NotNull String userId,
            @NotNull M model
    ) throws Exception;

    void clear(@NotNull String userId) throws Exception;

    boolean existsById(
            @NotNull String userId,
            @NotNull String id
    ) throws Exception;

    @Nullable
    List<M> findAll(@NotNull String userId) throws Exception;

    @Nullable
    List<M> findAll(
            @NotNull String userId,
            @Nullable Comparator comparator
    ) throws Exception;

    @Nullable
    M findOneById(
            @NotNull String userId,
            @NotNull String id
    ) throws Exception;

    int getSize(@NotNull String userId) throws Exception;

    void remove(
            @NotNull String userId,
            @NotNull M model
    ) throws Exception;

    void update(
            @NotNull String userId,
            @NotNull M model
    ) throws Exception;

}

package ru.tsc.denisturovsky.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name
    ) throws Exception;

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @NotNull Date dateBegin,
            @NotNull Date dateEnd
    ) throws Exception;

    @Nullable
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    ) throws Exception;

}

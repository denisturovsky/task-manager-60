package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.denisturovsky.tm.api.service.IServerPropertyService;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.constant.ContextTestData;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;
import ru.tsc.denisturovsky.tm.util.HashUtil;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserDTORepositoryTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private static final IServerPropertyService PROPERTY_SERVICE = ContextTestData.CONTEXT.getBean(
            IServerPropertyService.class);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.add(USER_TEST);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void add() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        Assert.assertNotNull(repository.add(ADMIN_TEST));
        @Nullable final UserDTO user = repository.findOneById(ADMIN_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
    }

    @After
    public void after() throws Exception {
        @Nullable UserDTO user = USER_SERVICE.findByLogin(ADMIN_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void create() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        @NotNull final UserDTO user = repository.create(
                ADMIN_TEST_LOGIN, HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD));
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
    }

    @Test
    public void createWithEmail() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        @NotNull final UserDTO user = repository.create(
                ADMIN_TEST_LOGIN, HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD), ADMIN_TEST_EMAIL);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(ADMIN_TEST.getEmail(), user.getEmail());
    }

    @Test
    public void createWithRole() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        @NotNull final UserDTO user = repository.create(
                ADMIN_TEST_LOGIN, HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD), Role.ADMIN);
        Assert.assertEquals(ADMIN_TEST.getLogin(), user.getLogin());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void existsById() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        Assert.assertFalse(repository.existsById(NON_EXISTING_USER_ID));
        Assert.assertTrue(repository.existsById(USER_TEST.getId()));
    }

    @Test
    public void findByEmail() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        @Nullable final UserDTO user = repository.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void findByLogin() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        @Nullable final UserDTO user = repository.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @Test
    public void findOneById() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        Assert.assertNull(repository.findOneById(NON_EXISTING_USER_ID));
        @Nullable final UserDTO user = repository.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
    }

    @NotNull
    public IUserDTORepository getRepository() {
        return CONTEXT.getBean(IUserDTORepository.class);
    }

    @Test
    public void isEmailExists() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        Assert.assertTrue(repository.isEmailExists(USER_TEST.getEmail()));
    }

    @Test
    public void isLoginExists() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        Assert.assertTrue(repository.isLoginExists(USER_TEST.getLogin()));
    }

    @Test
    public void removeByUserId() throws Exception {
        @NotNull final IUserDTORepository repository = getRepository();
        repository.add(ADMIN_TEST);
        Assert.assertNotNull(repository.findOneById(ADMIN_TEST.getId()));
        repository.remove(ADMIN_TEST);
        Assert.assertNull(repository.findOneById(ADMIN_TEST.getId()));
    }

}
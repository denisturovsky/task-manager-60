package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.denisturovsky.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.denisturovsky.tm.api.service.dto.IUserDTOService;
import ru.tsc.denisturovsky.tm.dto.model.SessionDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.marker.UnitCategory;

import java.util.Collections;
import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ContextTestData.CONTEXT;
import static ru.tsc.denisturovsky.tm.constant.SessionTestData.*;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.tsc.denisturovsky.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(UnitCategory.class)
public final class SessionDTORepositoryTest {

    @NotNull
    private final static IUserDTOService USER_SERVICE = CONTEXT.getBean(IUserDTOService.class);

    @NotNull
    private static String USER_ID = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final UserDTO user = USER_SERVICE.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final UserDTO user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        if (user != null) USER_SERVICE.remove(user);
    }

    @Test
    public void addByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        Assert.assertNotNull(repository.add(USER_ID, USER_SESSION3));
        @Nullable final SessionDTO session = repository.findOneById(USER_ID, USER_SESSION3.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION3.getId(), session.getId());
        Assert.assertEquals(USER_ID, session.getUserId());
    }

    @After
    public void after() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        repository.clear(USER_ID);
    }

    @Before
    public void before() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        repository.add(USER_ID, USER_SESSION1);
        repository.add(USER_ID, USER_SESSION2);
    }

    @Test
    public void clearByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        repository.clear(USER_ID);
        Assert.assertEquals(0, repository.getSize(USER_ID));
    }

    @Test
    public void existsByIdByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        Assert.assertFalse(repository.existsById(USER_ID, NON_EXISTING_SESSION_ID));
        Assert.assertTrue(repository.existsById(USER_ID, USER_SESSION1.getId()));
    }

    @Test
    public void findAllByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<SessionDTO> sessions = repository.findAll(USER_ID);
        Assert.assertNotNull(sessions);
        Assert.assertEquals(2, sessions.size());
        sessions.forEach(session -> Assert.assertEquals(USER_ID, session.getUserId()));
    }

    @Test
    public void findOneByIdByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        Assert.assertNull(repository.findOneById(USER_ID, NON_EXISTING_SESSION_ID));
        @Nullable final SessionDTO session = repository.findOneById(USER_ID, USER_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER_SESSION1.getId(), session.getId());
    }

    @NotNull
    public ISessionDTORepository getRepository() {
        return CONTEXT.getBean(ISessionDTORepository.class);
    }

    @Test
    public void getSizeByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        Assert.assertEquals(2, repository.getSize(USER_ID));
    }

    @Test
    public void removeByUserId() throws Exception {
        @NotNull final ISessionDTORepository repository = getRepository();
        repository.remove(USER_ID, USER_SESSION2);
        Assert.assertNull(repository.findOneById(USER_SESSION2.getId()));
    }

}
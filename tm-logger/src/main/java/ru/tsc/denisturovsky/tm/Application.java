package ru.tsc.denisturovsky.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.denisturovsky.tm.component.LoggerBootstrap;
import ru.tsc.denisturovsky.tm.configuration.LoggerConfiguration;

public final class Application {

    @SneakyThrows
    public static void main(@Nullable String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final LoggerBootstrap loggerBootstrap = context.getBean(LoggerBootstrap.class);
        loggerBootstrap.init();
    }

}
